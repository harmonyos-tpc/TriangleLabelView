TriangleLabelView
====================
Show triangle Component.
How to Use
=====
## Gradle Dependency

```
implementation "io.openharmony.tpc.thirdlib:TriangleLabelView:1.0.1"
```
To see how the TriangleLabelView are added to your xml layouts, check the sample project.

```
<jp.shts.android.library.TriangleLabelView
        ohos:id="$+id:left_top_no_1111"
        ohos:width="match_parent"
        ohos:height="match_parent"

        ohos:align_parent_left="true"
        ohos:align_parent_top="true"

        ohos:backgroundColor="#F57F17"
        ohos:corner="leftTop"
        ohos:labelBottomPadding="5vp"
        ohos:labelCenterPadding="0vp"
        ohos:labelTopPadding="10vp"
        ohos:primaryText="New"
        ohos:primaryTextColor="#FFEB3B"
        ohos:primaryTextSize="16fp"
        ohos:secondaryText="01"
        ohos:secondaryTextColor="#FFF9C4"
        ohos:secondaryTextSize="11fp" />

```

License
=======

    Copyright (C) 2016 Shota Saito

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
